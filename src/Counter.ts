import {createElement} from '~/createElement';
import CounterManager from '~/CounterManager';
import './counter.css';

const WARNING_TIME = 60 * 1000;

export default class Counter {
  name: string;
  ends: number;
  bg: string;
  counterManager: CounterManager;
  paused: boolean = false;
  secondsLeft: number = 0;
  private root: HTMLElement;
  private timer: HTMLElement;

  constructor(counterManager: CounterManager, name: string, ends: number, bg: string) {
    this.name = name;
    this.ends = ends;
    this.bg = bg;
    this.timer = this.createTimer();
    this.counterManager = counterManager;
    this.root = createElement(
      'div',
      {className: `counter ${this.bg}`},
      [
        createElement(
          'div',
          {className: 'buttons'},
          [
            createElement(
              'div',
              {className: 'remove', onClick: this.remove},
              null
            ),
            createElement(
              'div',
              {className: 'pause', onClick: this.togglePause},
              null
            )
          ]
        ),
        createElement('div', {}, this.name),
        this.timer
      ]
    );
    this.counterManager.addCounter(this);
  }

  getRoot = (): HTMLElement => this.root;

  getRemaining = (): string => {
    const date = new Date(this.ends - Date.now());
    const h = leftPad(date.getUTCHours());
    const m = leftPad(date.getUTCMinutes());
    const s = leftPad(date.getUTCSeconds());
    return `${h}:${m}:${s}`;
  };

  getClassName = (): string => {
    if (this.ends - Date.now() < 1000) return 'ended';
    if (this.ends - Date.now() < WARNING_TIME) return 'warning';
    return '';
  };

  remove = (): void => {
    this.counterManager.removeCounter(this);
    this.root.remove();
  };

  createTimer = (): HTMLElement => {
    return createElement(
      'div',
      {className: this.getClassName()},
      this.getRemaining()
    );
  };

  updateTimer = (): void => {
    const next = this.createTimer();
    this.root.replaceChild(
      next,
      this.timer
    );
    this.timer = next;
  };

  shouldUpdate = (): boolean => this.ends >= Date.now();

  pause = () => {
    this.paused = true;
    this.secondsLeft = this.ends - Date.now();
    this.root.classList.add('paused');
  };

  unpause = () => {
    this.ends = this.secondsLeft + Date.now();
    this.paused = false;
    this.secondsLeft = 0;
    this.root.classList.remove('paused');
  };

  togglePause = () => {
    if (this.paused) this.unpause();
    else this.pause();
  };
}

function leftPad(n: number) {
  return `0${n}`.slice(-2);
}
