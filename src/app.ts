import {createElement} from '~/createElement';
import CounterManager from '~/CounterManager';
import './app.css';

const DEFAULT_NAME = () => `counter ${Math.random().toString(16).slice(-4)}`;
const DEFAULT_TIME = () => 1800;

export default function app(root: HTMLElement | null) {
  if (!root) throw new Error('no root');
  const counterManager = new CounterManager();

  const inputName = createElement('input', {type: 'text', placeholder: 'Название счётчика'}, null);
  const inputCount = createElement('input', {type: 'number', placeholder: 'Секунд до конца'}, null);
  const button = createElement('button', {onClick: addCounter}, 'Добавить');
  const countersBox = createElement(
    'div',
    {className: 'counters'},
    null
  );
  const inputsBox = createElement(
    'div',
    {className: 'inputs'},
    [
      inputName,
      inputCount,
      button
    ]
  );
  const app = createElement(
    'div',
    {},
    [
      inputsBox,
      countersBox
    ]
  );

  root.appendChild(app);

  counterManager.run();

  function addCounter(): void {
    if (counterManager.counters.length >= 4) return;
    const name = inputName.value || DEFAULT_NAME();
    const ends = Date.now() + ((Number(inputCount.value) || DEFAULT_TIME()) * 1000);
    const counter = counterManager.createCounter(name, ends);
    countersBox.appendChild(counter.getRoot());
    inputName.value = '';
    inputCount.value = '';
  }
}
