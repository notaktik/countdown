type Tag = 'div' | 'span' | 'button' | 'input';

const ATTRS = [
  'type',
  'placeholder'
];

export function createElement<T extends Tag>(
  tag: T,
  options: any,
  children: Array<HTMLElement | string> | HTMLElement | string | null
) {
  const elem = document.createElement(tag);

  // options
  Object.keys(options).forEach(key => {
    if (key === 'className' && options[key]) {
      elem.classList.add(...options[key].split(' '));
    }
    if (key.startsWith('on')) {
      elem.addEventListener(key.slice(2).toLowerCase(), options[key]);
    }
    if (ATTRS.includes(key)) {
      elem.setAttribute(key, options[key]);
    }
  });

  // children
  if (Array.isArray(children)) {
    children.map(append);
  } else {
    append(children);
  }
  return elem;

  function append(child: HTMLElement | string | null) {
    if (typeof child === 'string') {
      elem.innerText = child;
    } else if (child instanceof HTMLElement) {
      elem.appendChild(child);
    }
  }
}
