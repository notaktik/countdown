import Counter from '~/Counter';

export default class CounterManager {
  backgrounds: Record<string, boolean> = {
    'grey': false,
    'red': false,
    'green': false,
    'blue': false
  };
  counters: Array<Counter> = [];

  createCounter = (name: string, ends: number): Counter => {
    let bg = 'grey';
    for (const key in this.backgrounds) {
      if (!this.backgrounds.hasOwnProperty(key)) continue;
      if (!this.backgrounds[key]) {
        bg = key;
        this.backgrounds[key] = true;
        break;
      }
    }
    return new Counter(this, name, ends, bg);
  };

  addCounter = (counter: Counter): void => {
    this.counters.push(counter);
  };

  removeCounter = (counter: Counter): void => {
    this.counters = this.counters.filter(_counter => _counter !== counter);
    this.backgrounds[counter.bg] = false;
  };

  update = (): void => {
    this.counters.forEach(counter => {
      if (!counter.paused && counter.shouldUpdate()) counter.updateTimer();
    });
  };

  run = (): void => {
    let prev = now();

    const tick = () => {
      const cur = now();
      if (prev < cur) {
        prev = cur;
        this.update();
      }
      requestAnimationFrame(tick);
    };

    requestAnimationFrame(tick);

    function now(): number {
      return Math.round(Date.now() / 1000);
    }
  };
}
